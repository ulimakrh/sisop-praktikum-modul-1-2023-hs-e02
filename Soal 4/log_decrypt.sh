#!/bin/bash
cd "/home/razanathallah/Documents/SISOP/M1"

echo "input nama file yang akan di dekripsi:"
read file

hour=$(echo "$file" | cut -c1)
hour+=$(echo "$file" | cut -c2)
a=${hour:0:1}
b=${hour:1:1}
count=$((10*a+b))

date=$(date "+%H:%M %d:%m:%Y")
backup_file="$date.txt"

alpha="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
dec_alpha=
for ((i=0; i<${#alpha}; i++))
do
        char=${alpha:$i:1}
        asc=$(printf '%d' "'$char")
        dec_asc=$((asc-count))
        if [[ $char =~ [a-z] && $dec_asc -lt 97 ]] || [[ $char =~ [A-Z] && $dec_asc -lt 65 ]]
        then
                dec_asc=$((dec_asc+26))
        fi
        dec_char=$(printf \\$(printf '%03o' "$dec_asc"))
        dec_alpha+=$dec_char
done

cat "$file" | tr "${alpha}" "${dec_alpha}" > "${backup_file}"
