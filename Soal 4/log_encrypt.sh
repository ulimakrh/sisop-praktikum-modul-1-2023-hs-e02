#!/bin/bash
cd "/home/razanathallah/Documents/SISOP/M1"
date=$(date "+%H:%M %d:%m:%Y")
hour=$(date "+%H")
a=${hour:0:1}
b=${hour:1:1}
count=$((10*a+b))
backup_file="$date.txt"

alpha="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
enc_alpha=
for ((i=0; i<${#alpha}; i++))
do
	char=${alpha:$i:1}
	asc=$(printf '%d' "'$char")
	enc_asc=$((asc+count))
	if [[ $char =~ [a-z] && $enc_asc -gt 122 ]] || [[ $char =~ [A-Z] && $enc_asc -gt 90 ]]
	then
		enc_asc=$((enc_asc-26))
	fi
	enc_char=$(printf \\$(printf '%03o' "$enc_asc"))
	enc_alpha+=$enc_char
done

cat /var/log/syslog | tr "${alpha}" "${enc_alpha}" > "${backup_file}"

min=$(date "+%M")
count=$((count+2))
count=$((count%24))
crontab -l | grep -v "log_encrypt.sh" > cron.txt
echo "$min $count * * * /home/razanathallah/Documents/SISOP/M1/log_encrypt.sh" >> cron.txt
crontab cron.txt
rm cron.txt
