#!/bin/bash
cd "/home/razanathallah/Documents/SISOP/M1"

downloadImage(){
	hour=$(date "+%H")
	a=${hour:0:1}
	b=${hour:1:1}
	count=$((10*a+b))
	if [[ $count -eq 0 ]]
	then
		((count++))
	fi

        min=$(date "+%M")
	url="https://www.its.ac.id/wp-content/uploads/2017/09/Gambar2111-1-1024x683.jpg"

	foldercounter=1
	while true
	do
		foldertocheck="kumpulan_$foldercounter"
		folderpath=$(find ~/Documents/SISOP/M1 -name "$foldertocheck")
		if [ -z "$folderpath" ]
		then
			break;
		else
			((foldercounter++))
		fi
	done

	loc="$HOME/Documents/SISOP/M1/kumpulan_$foldercounter/"
	mkdir -p "$loc"

	for ((i=1; i<=$count; i=i+1))
	do
		filename="$loc/perjalanan_$i"
		wget -O "$filename" "$url"
	done

	count=$((count+10))
	count=$((count%24))
	crontab -l | grep -v "kobeni_liburan.sh downloadImage" > cron.txt
	echo "$min $count * * * /home/razanathallah/Documents/SISOP/M1/kobeni_liburan.sh downloadImage" >> cron.txt
	crontab cron.txt
	rm cron.txt
}

zipFolder(){
	zipcounter=1
	while true
	do
		ziptocheck="devil_$zipcounter.zip"
		zippath=$(find ~/Documents/SISOP/M1 -name "$ziptocheck")
		if [ -z "$zippath" ]
		then
			zipname="${ziptocheck}"
       			find ~/Documents/SISOP/M1 -type d -name "kumpulan*" -execdir zip -r "$zipname" {} \;
			break
		else
			((zipcounter++))
		fi
	done

	hour=$(date "+%H")
    min=$(date "+%M")
	crontab -l | grep -v "kobeni_liburan.sh zipFolder" > cron.txt
	echo "$min $hour * * * /home/razanathallah/Documents/SISOP/M1/kobeni_liburan.sh zipFolder" >> cron.txt
    crontab cron.txt
    rm cron.txt
}

if [[ $1 == "downloadImage" ]]
then
	downloadImage
fi

if [[ $1 == "zipFolder" ]]
then
	zipFolder
fi
