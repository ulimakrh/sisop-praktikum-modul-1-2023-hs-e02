# Praktikum 1 Sistem Operasi 2023

Kelompok E02

1. 5025211008 - Muhammad Razan Athallah
2. 5025211232 - Ulima Kaltsum Rizky Hibatullah
3. 5025211137 - Kalyana Putri Al Kanza

### Soal 1
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
- Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
- Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 
- Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
- Bocchi ngefans berat dengan universitas paling keren di dunia

##### Penyelesaian
Terdapat 4 subsoal pada soal nomor 1 ini, yang berkaitan dengan pengurutan data tertentu berdasarkan file '2023 QS World University Rankings.csv'.
- Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
```sh
sort -t ',' -k 1n '2023 QS World University Rankings.csv' | awk -F ',' '/JP/{print$1,$2}' | head -5
printf "\n" 
```
Perintah `sort -t ‘,’` pada code digunakan untuk mengurutkan baris dalam sebuah file teks yang menggunakan delimiter berupa tanda koma. `-k 1n` akan membuat perintah sort melakukan pengurutan berdasarkan kolom pertama dan menggunakan pengurutan numerik (tipe pengurutan "n") secara ascending (dari kecil ke besar). Dalam code tersebut, `awk -F ‘,’` akan membentuk kolom dari file .csv dengan value yang dipisahkan tanda koma. Lalu, `‘/JP/{print$1,$2}’` akan mencari dan mencetak data pada kolom 1 (ranking) dan kolom 2 (nama institusi) dalam file .csv yang memiliki kata kunci JP (Jepang) dan `head -5` akan mencetak 5 universitas di Jepang dengan ranking tertinggi.

Berikut merupakan keluaran dari code diatas:
![Screenshot_from_2023-03-10_20-43-12](/uploads/361888d918e02fdcd5efada0445aea32/Screenshot_from_2023-03-10_20-43-12.png)



- Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang.

```sh
awk -F ',' '/JP/{print$9,$2}' '2023 QS World University Rankings.csv' | sort -t ',' -g | head -5
printf "\n"
```
Dalam code tersebut, `awk -F ‘,’` akan membentuk kolom dari file .csv dengan value yang dipisahkan tanda koma. Simbol pipe atau | digunakan untuk melanjutkan argumen sebelumnya dan `sort -t ‘,’ -g` akan mengurutkan baris dalam sebuah file teks yang menggunakan delimiter berupa tanda koma berdasarkan nilai numerik dan bukan alfabetikal. Lalu, `‘/JP/{print$9,$2}’` akan mencari dan mencetak data pada kolom 9 (fsr score) dan kolom 2 (nama institusi) dalam file .csv yang memiliki kata kunci JP (Jepang) dan `head -5` akan mencetak 5 universitas di Jepang dengan fsr score terendah.

Berikut merupakan keluaran dari code diatas:

![Screenshot_from_2023-03-10_20-43-21](/uploads/1ab7763c6a349b7ae32af27afee82cf0/Screenshot_from_2023-03-10_20-43-21.png)

- Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
```sh
sort -t ',' -k 20n '2023 QS World University Rankings.csv' | awk -F ',' '/JP/{print$20,$2}' | head -10
printf "\n"
```
Secara garis besar, penyelesaian untuk soal ini hampir sama dengan soal-soal sebelumnya, di mana perbedaannya hanya terletak pada kolom dan jumlah universitas yang akan dicetak. Soal meminta kita untuk mencetak 10 universitas di Jepang dengan ger rank paling tinggi, sehingga sesuai dengan code yang ada `‘/JP/{print$20,$2}'`, kolom yg diprint adalah kolom 20 (ger rank) dan kolom 2 (nama institusi) yang memiliki kode JP di dalamnya dan `head -10` untuk mencetak sebanyak 10 nama.

Berikut adalah keluaran dari code diatas:
![Screenshot_from_2023-03-10_20-43-32](/uploads/c06d1a476b83031f912ee39fd6adf8d5/Screenshot_from_2023-03-10_20-43-32.png)

- Bocchi ngefans berat dengan universitas paling keren di dunia
```sh
grep -i 'keren' '2023 QS World University Rankings.csv' | awk -F ',' '{print$2}'
printf "\n"
```
Pada soal ini, digunakan syntax yang berbeda namun lebih sederhana. Grep digunakan untuk mencari teks atau pola tertentu dalam file dan `grep -i` akan melakukan pencarian dengan tidak memperhatikan perbedaan huruf besar atau kecil, sehingga perintah grep dapat menemukan semua kemunculan kata ‘keren’ di dalam file .csv. Lalu, `{print$2}` akan mencetak nama institusi sesuai isi kolom kedua yang memiliki kata kunci 'keren'.

Berikut merupakan keluaran dari code diatas:
![Screenshot_from_2023-03-10_20-43-40](/uploads/f5d0c4bc5d87c4308b4b91f5fdf8eb2f/Screenshot_from_2023-03-10_20-43-40.png)

### Soal 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 
- Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
    - File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
    - File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 
- Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

##### Penyelesaian
Pada soal ini, diberikan dua tugas utama dengan perulangan waktu yang berbeda, yaitu:
1. Download gambar sebanyak digit jam dan disatukan kedalam sebuah folder setiap 10 jam.
2. Zip folder-folder yang berisi gambar setiap 24 jam.

Karena kedua tugas ini harus diletakkan dalam satu script, maka dibuatlah dua fungsi yaitu `downloadImage()` dan `zipFolder()`. Hanya saja ketika melakukan execute perlu ditambahkan satu argumen yaitu downloadImage atau zipFolder.

- Download gambar (downloadImage)
```sh
downloadImage(){
	hour=$(date "+%H")
	a=${hour:0:1}
	b=${hour:1:1}
	count=$((10*a+b))
	if [[ $count -eq 0 ]]
	then
		((count++))
	fi 
```
Pada fungsi downloadImage, terlebih dahulu harus ditentukan jumlah image yang harus didownload yaitu dengan mengambil digit-digit dari jam menggunakan command `date`. 
Baris pertama dari fungsi downloadImage mengatur variabel `hour` ke waktu saat ini dengan format 24 jam. Misalnya, jika saat ini jam menunjukkan pukul 1.15 siang, maka jam akan disetel ke "13.15" (karena 13.15 adalah representasi waktu 24 jam dari jam 1.15 siang). 

Lalu, program akan mengambil karakter pertama dan kedua dari string `hour` dan menyimpannya masing-masing ke dalam variabel a dan b. Misalnya, jika hour = 13, a bernilai 1 dan b bernilai 3.

Variabel `count` kemudian diset menjadi a x 10 + b. Misalnya, jika hour bernilai 13, maka count akan diset ke 13, dan if statement akan mengecek apabila count bernilai 0 yang menandakan jam menunjukkan pukul 00, maka gambar didownload satu kali sehingga hasil hitung diinkremen.

```sh
min=$(date "+%M")
url="https://www.its.ac.id/wp-content/uploads/2017/09/Gambar2111-1-1024x683.jpg"

```
Fungsi mendapatkan nilai menit menggunakan perintah `date` dan menyimpannya dalam variabel min. Variabel url berisi URL gambar yang akan diunduh.

```sh
foldercounter=1
while true
do
	foldertocheck="kumpulan_$foldercounter"
	folderpath=$(find ~/Documents/SISOP/M1 -name 	"$foldertocheck")
	if [ -z "$folderpath" ]
	then
		break;
	else
		((foldercounter++))
	fi
done
```
Langkah selanjutnya adalah membuat folder sesuai ketentuan soal. Karena nama folder harus urut (kumpulan_1, kumpulan_2, ...), maka perlu dicari tahu terlebih dahulu nama folder yang selanjutnya akan dibuat. Pada script, variabel `foldercounter` digunakan untuk menandakan angka pada nama folder yang akan dicari, variabel `foldertocheck` berisi nama folder yang akan dicari, dan variabel `folderpath` akan digunakan untuk menampung path dari folder yang dicari dengan bantuan command `find`. Jika variabel folderpath tidak berisi, maka folder yang dicari akan menjadi folder yang selanjutnya dibuat. Folder dibuat dengan menggunakan command `mkdir`.

Untuk melakukan pengecekan, pertama foldercounter diinisialisasi dengan nilai 1. Lalu, setiap iterasi akan mengecek apakah folder dengan nama "kumpulan_..." yang diikuti dengan nomor urut yang disimpan pada foldercounter sudah ada di dalam direktori. Jika folder belum ditemukan, maka proses pencarian dihentikan dengan menggunakan command `break`. Namun jika sudah, foldercounter akan ditambahkan 1 untuk memeriksa folder selanjutnya dengan nama "kumpulan_" diikuti dengan nomor urut yang lebih besar.

```sh
loc="$HOME/Documents/SISOP/M1/kumpulan_$foldercounter/"
mkdir -p "$loc"

for ((i=1; i<=$count; i=i+1))
do
	filename="$loc/perjalanan_$i"
	wget -O "$filename" "$url"
done
```
Proses download gambar dilakukan dengan looping menggunakan index i yang juga digunakan sebagai nama dari file gambar. Command `wget -O` digunakan untuk mendownload gambar dan meletakkannya di path yang kita tentukan.

```sh
count=$((count+10))
count=$((count%24))
crontab -l | grep -v "kobeni_liburan.sh downloadImage" > cron.txt
echo "$min $count * * * /home/razanathallah/Documents/SISOP/M1/kobeni_liburan.sh downloadImage" >> cron.txt
crontab cron.txt
rm cron.txt
}
```
Supaya download gambar dijalankan setiap 10 jam sekali, maka diperlukan cron job. Untuk menentukan menit, maka digunakan lagi fungsi `date`. Untuk jam, dapat diambil dari hasil hitung di awal ditambah dengan 10 dan menghitung modulus dari variabel count dengan 24 agar jam tetap berada dalam range 0-23.

Selanjutnya, ambil list cron yang sudah ada dengan command `crontab -l`, kemudian hapus semua line yang memiliki kata downloadImage dengan command `grep -v`, dan dimasukkan kedalam temporary file cron.txt dengan redirection `>`. Cron yang baru akan diappend ke file cron.txt dengan command echo dan redirection `>>`. List cron pada cron.txt kemudian diexecute dengan command crontab, serta file cron.txt dihapus dengan command `rm`.

berikut adalah hasil download gambar beserta pembuatan folder, dapat dilihat bahwa jika folder kumpulan_1 sudah ada maka yang dibuat adalah folder kumpulan_2.
![Screenshot_from_2023-03-10_17-27-25](/uploads/76c19bf199a18195f7225d51d276a2d2/Screenshot_from_2023-03-10_17-27-25.png)
![Screenshot_from_2023-03-10_17-28-13](/uploads/9c3a4eba70e39fd7568d69188b772cf3/Screenshot_from_2023-03-10_17-28-13.png)


- Zip folder (zipFolder)
```sh
zipFolder(){
	zipcounter=1
	while true
	do
		ziptocheck="devil_$zipcounter.zip"
		zippath=$(find ~/Documents/SISOP/M1 -name "$ziptocheck")
		if [ -z "$zippath" ]
		then
			zipname="${ziptocheck}"
       			find ~/Documents/SISOP/M1 -type d -name "kumpulan*" -execdir zip -r "$zipname" {} \;
			break
		else
			((zipcounter++))
		fi
	done

    hour=$(date "+%H")
    min=$(date "+%M")
    crontab -l | grep -v "kobeni_liburan.sh zipFolder" > cron.txt
    echo "$min $hour * * * /home/razanathallah/Documents/SISOP/M1/kobeni_liburan.sh zipFolder" >> cron.txt
    crontab cron.txt
    rm cron.txt
}
```
Pada fungsi zipFolder, langkah pertama yang dilakukan adalah menentukan nama zip yang selanjutnya dibuat, dimana langkah tersebut kurang lebih sama dengan pencarian nama folder pada fungsi downloadImage. Ketika nama zip sudah ditemukan maka dilakukan pencarian semua folder yang akan dizip dengan command `find` dan option `-type d` serta `-name`. Command `-execdir` digunakan untuk mengeksekusi perintah `zip -r` pada hasil find.

Berikut adalah hasil zip yang berisi folder kumpulan_1 dan kumpulan_2:
![Screenshot_from_2023-03-10_17-28-02](/uploads/7af86b334ae1e22b7ce2c1433dacdf9a/Screenshot_from_2023-03-10_17-28-02.png)

```sh
if [[ $1 == "downloadImage" ]]
then
	downloadImage
fi

if [[ $1 == "zipFolder" ]]
then
	zipFolder
fi
```
Terakhir, conditional statement untuk memilih file mana yang akan dijalankan sesuai argumen yang dimasukkan.

Berikut adalah list cron setelah downloadImage dan zipFolder diexecute:
![Screenshot_from_2023-03-10_17-29-05](/uploads/fffe79b562b51a9dab559a20cd9e7e90/Screenshot_from_2023-03-10_17-29-05.png)

##### Kendala Pengerjaan
Kendala yang dihadapi saat pengerjaan soal ini adalah pembacaan jam. Jam 9 pagi dibaca sebagai string '09'. Solusi yang kami lakukan adalah menerjemahkan string ini menjadi angka sesuai code yang sudah dijelaskan diatas Sehingga jam yang terbaca adalah bilangan bulat dan download gambar dapat dilakukan sebagaimana seharusnya.

### Soal 3
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh
- Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
    - Minimal 8 karakter
    - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    - Alphanumeric
    - Tidak boleh sama dengan username 
    - Tidak boleh menggunakan kata chicken atau ernie
- Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
    - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
    - Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
    - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
    - Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

##### Penyelesaian
Soal meminta sebuah program di mana pengguna dapat melakukan sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. dan sistem login yang dibuat di script retep.sh, dengan beberapa ketentuan pada username dan passwordnya.

- Register (louis.sh)
```sh
while grep -q "^${username} " ./users/users.txt; do   
    echo "$(date +"%y/%m/%d %T") REGISTER: ERROR User already exists" >> ./users/log.txt
    echo "Username not valid. Input your username again"
    read username
done

echo "Input your password"
read password
```
Loop while berfungsi untuk mengecek file users.txt apakah ada baris yang berisi string yang sama dengan nilai dari variabel username. Jika ada, maka akan muncul pesan "ERROR User already exists". Kemudian, program akan meminta pengguna memasukkan username baru dan membaca input dari pengguna menggunakan perintah read. Loop `while` akan terus berjalan hingga tidak ditemukan baris yang dimulai dengan string yang sama dengan nilai variabel username. Lalu, pengguna diminta memasukkan password dan program akan membaca input dari pengguna menggunakan perintah `read`.

Selanjutnya, terdapat beberapa ketentuan password yang dapat digunakan oleh pengguna dan loop `until` akan membuat program terus berjalan hingga kondisi yang diberikan terpenuhi.
```sh
until [[ ${#password} -gt 7 ]] && [[ "$password" =~ [A-Z] ]] && [[ "$password" =~ [a-z] ]] && [[ "$password" =~ [0-9] ]] && [[ ! "$password" == "$username" ]] && [[ ! "$password" == "chicken" ]] && [[ ! "$password" == "ernie" ]]; do
```
1. Password tidak boleh sama dengan username
```sh
if [[ "$password" == "$username" ]]; then
    echo "Password must not be the same as username"
fi
```
2. Panjang password harus >7 karakter (minimal 8)
```sh
if [[ ! ${#password} -gt 7 ]]; then
    echo "Password must be at least 8 characters long"
fi
```
3. Password harus mengandung minimal 1 huruf kecil (lowercase)
```sh
if [[ ! "$password" =~ [a-z] ]]; then
    echo "Password must contain at least one lowercase letter"
fi
```
4. Password harus mengandung minimal 1 huruf besar (uppercase)
```sh
if [[ ! "$password" =~ [A-Z] ]]; then
    echo "Password must contain at least one uppercase letter"
fi
```
5. Password harus mengandung minimal 1 angka 
```sh
if [[ ! "$password" =~ [0-9] ]]; then
    echo "Password must contain at least one number"
fi
```
6. Password tidak boleh mengandung kata chicken atau ernie
```sh
if [[ "$password" == "chicken" ]]; then
    echo "Password must not contain the word 'chicken'"
fi
if [[ "$password" == "ernie" ]]; then
    echo "Password must not contain the word 'ernie'"
fi
```
Setelah password memenuhi segala ketentuan, perintah selanjutnya akan dieksekusi.
```sh
echo "$(date +"%y/%m/%d %T") REGISTER: INFO User $username registered successfully" >> ./users/log.txt
echo $username $password >> ./users/users.txt
```
Pertama, informasi tentang pendaftaran pengguna akan dimasukkan ke file log.txt pada folder "users"dan berisi informasi waktu (tanggal dan waktu saat ini) dan username pengguna yang berhasil terdaftar.
Kedua, informasi tadi akan dimasukkan ke file users.txt pada folder "users"dan akan digunakan untuk memvalidasi login pengguna pada proses selanjutnya.

- Login (retep.sh)

Melalui perintah `echo` dan `read`, pengguna diminta untuk memasukkan username.
```sh
echo "Input your username"
read username
```
Program akan memeriksa apakah username yang dimasukkan pengguna sudah terdaftar di dalam file "users.txt". Jika belum terdaftar, program akan menampilkan pesan "User not found" dan proses login akan dihentikan. Perintah
`grep` dan `cut` akan mengambil password yang terdaftar pada file "users.txt" dan akan disimpan dalam variabel `user_password`.

```sh
if ! grep -q "^${username} " ./users/users.txt; then
    echo "User not found"
else
    user_password=$(grep "^${username} " ./users/users.txt | cut -d' ' -f2)
    echo "Input your password"
    read password
```

Selanjutnya, sistem akan memeriksa apakah password yang dimasukkan pengguna sesuai dengan password yang terdaftar di dalam file "users.txt". Jika tidak sesuai, sistem akan menampilkan pesan "Incorrect password" dan mencatat kegagalan pada file log.txt dengan `echo` dan `date`. Jika password sesuai, maka sistem akan menampilkan pesan "Login successful" dan mencatat informasi login yang berhasil pada file log.txt.

```sh
if [[ "$password" != "$user_password" ]]; then
    echo "Incorrect password"
    echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >> ./users/log.txt
else
    echo "Login successful"
    echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >> ./users/log.txt
fi
```

Berikut adalah isi dari file log.txt dan users.txt:
![Screenshot_from_2023-03-10_21-23-12](/uploads/0eb4972ee067549a7150dc9a725bb889/Screenshot_from_2023-03-10_21-23-12.png)
![Screenshot_from_2023-03-10_21-23-03](/uploads/47886c4d95369988b08224b4c6653f8b/Screenshot_from_2023-03-10_21-23-03.png)

### Soal 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
    - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
    - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    - Setelah huruf z akan kembali ke huruf a
- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan 😀.

##### Penyelesaian
Pada soal ini, diberikan dua tugas yaitu:
1. Melakukan backup enkripsi dari file syslog setiap 2 jam.
2. Melakukan dekripsi untuk hasil backup.

- Enkripsi (log_encrypt.sh)

Script log_encrypt.sh berisi command-command untuk melakukan backup enkripsi. Pertama-tama dibuat dulu nama dari backup file dengan menggunakan command `date`. Setelah itu, digit-digit pada jam diambil sebagai key dari enkripsi.


```sh
date=$(date "+%H:%M %d:%m:%Y")
hour=$(date "+%H")
a=${hour:0:1}
b=${hour:1:1}
count=$((10*a+b))
backup_file="$date.txt"
```
Potongan code tersebut mengambil waktu saat ini menggunakan command date dan menyimpannya ke dalam variabel `date`. Selain itu, kode ini juga melakukan manipulasi pada waktu yang didapat dari command date untuk menghasilkan sebuah nama file backup yang akan digunakan. Nilai variabel count akan digunakan untuk menentukan urutan backup yang dibuat.

```sh
alpha="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
enc_alpha=
```
Dibuatlah dua string di mana string pertama `alpha` berisi huruf alfabet kecil dan besar secara terurut, sedangkan string kedua `enc_alpha` merupakan string kosong yang nantinya akan diisi dengan hasil enkripsi.

```sh
for ((i=0; i<${#alpha}; i++))
do
	char=${alpha:$i:1}
	asc=$(printf '%d' "'$char")
	enc_asc=$((asc+count))
	if [[ $char =~ [a-z] && $enc_asc -gt 122 ]] || [[ $char =~ [A-Z] && $enc_asc -gt 90 ]]
	then
		enc_asc=$((enc_asc-26))
	fi
	enc_char=$(printf \\$(printf '%03o' "$enc_asc"))
	enc_alpha+=$enc_char
done
```
Setiap karakter pada string pertama akan diubah terlebih dahulu menjadi angka desimal sesuai ketentuan ASCII, kemudian ditambahkan dengan key yang sudah ada. Selanjutnya, angka desimal yang melebihi desimal dari z dan Z akan dikurangi dengan 26. Berikutnya angka desimal dikembalikan menjadi bentuk karakter dan diappend ke string kedua. Setelah perintah for selesai dijalankan, hasil enkripsi akan disimpan dalam variabel `enc_alpha`.
```sh
cat /var/log/syslog | tr "${alpha}" "${enc_alpha}" > "${backup_file}"
```
Isi dari file syslog dibaca dengan menggunakan command `cat`. Kemudian command `tr` digunakan untuk mengubah semua karakter yang sesuai dengan string pertama menjadi karakter pada string kedua. Setelah itu hasilnya dimasukkan ke backup file menggunakan redirection `>`.

Berikut merupakan hasil dari enkripsi pada pukul 17:29 pada tanggal 10 Maret 2023:
![Screenshot_from_2023-03-10_17-29-50](/uploads/324481110f4050bca0028dce485bc693/Screenshot_from_2023-03-10_17-29-50.png)

![Screenshot_from_2023-03-10_17-29-59](/uploads/d4d8dd80bde02bcc94afe9e536b6ae35/Screenshot_from_2023-03-10_17-29-59.png)

Untuk cron, langkah yang digunakan sangat mirip dengan cron pada soal nomor 2.
```sh
min=$(date "+%M")
count=$((count+2))
count=$((count%24))
crontab -l | grep -v "log_encrypt.sh" > cron.txt
echo "$min $count * * * /home/razanathallah/Documents/SISOP/M1/log_encrypt.sh" >> cron.txt
crontab cron.txt
rm cron.txt
```
Program akan menambahkan nilai count sebanyak 2, lalu menghitung sisa hasil bagi variabel count dengan 24 (sehingga count akan selalu bernilai antara 0-23).

Berikut adalah list cron setelah file log_encrypt.sh dieksekusi:
![Screenshot_from_2023-03-10_17-44-15](/uploads/f7cb08073c4ae217fb804d7346cfb95a/Screenshot_from_2023-03-10_17-44-15.png)


- Dekripsi (log_decrypt.sh)

Terlebih dahulu pengguna diminta untuk memasukkan nama file yang akan dienkripsi dengan menggunakan command echo, serta nama file dicatat dengan command read.

```sh
echo "input nama file yang akan di dekripsi:"
read file
```

Pada dasarnya, dekripsi merupakan kebalikan dari enkripsi. Letak perbedaan mereka berada pada potongan code berikut.

**Enkripsi**
```sh
if [[ $char =~ [a-z] && $enc_asc -gt 122 ]] || [[ $char =~ [A-Z] && $enc_asc -gt 90 ]]
	then
		enc_asc=$((enc_asc-26))
```
Karakter yang dienkripsi akan dicek apakah melebihi berada dalam rentang batas ASCII yaitu 122 (lowercase) dan 90 (uppercase). Jika karakter tersebut melebihi batas tersebut, maka ASCII value karakter tersebut akan dikurangi 26 dan kembali ke urutan alfabet awal.

**Dekripsi**
```sh
if [[ $char =~ [a-z] && $dec_asc -lt 97 ]] || [[ $char =~ [A-Z] && $dec_asc -lt 65 ]]
        then
                dec_asc=$((dec_asc+26))
```
Sementara pada dekripsi, karakter harus berada pada rentang nilai ASCII kurang dari 97 (a) dan 65(A), sehingga jika value karakter di luar rentang tersebut nilai ASCII akan ditambah dengan 26 agar kembali ke nilai ASCII huruf yang asli.

Berikut adalah hasil dari dekripsi file '17:29 10:03:2023.txt':
![Screenshot_from_2023-03-10_17-30-34](/uploads/2c172c3915aeba60624c61d8f7b29eec/Screenshot_from_2023-03-10_17-30-34.png)

![Screenshot_from_2023-03-10_17-30-42](/uploads/fa97c887bba19538ff024920cd3cd8c9/Screenshot_from_2023-03-10_17-30-42.png)
