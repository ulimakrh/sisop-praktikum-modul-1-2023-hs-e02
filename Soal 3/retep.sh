#!/bin/bash

echo "Input your username"
read username

if ! grep -q "^${username} " ./users/users.txt; then
    echo "User not found"
else
    user_password=$(grep "^${username} " ./users/users.txt | cut -d' ' -f2)
    echo "Input your password"
    read password

    if [[ "$password" != "$user_password" ]]; then
        echo "Incorrect password"
        echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >> ./users/log.txt
    else
        echo "Login successful"
        echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >> ./users/log.txt
    fi
fi
