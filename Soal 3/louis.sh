#!/bin/bash
echo "Input your username"
read username

while grep -q "^${username} " ./users/users.txt; do   
    echo "$(date +"%y/%m/%d %T") REGISTER: ERROR User already exists" >> ./users/log.txt
    echo "Username not valid. Input your username again"
    read username
done

echo "Input your password"
read password

until [[ ${#password} -gt 7 ]] && [[ "$password" =~ [A-Z] ]] && [[ "$password" =~ [a-z] ]] && [[ "$password" =~ [0-9] ]] && [[ ! "$password" == "$username" ]] && [[ ! "$password" == "chicken" ]] && [[ ! "$password" == "ernie" ]]; do
    if [[ "$password" == "$username" ]]; then
        echo "Password must not be the same as username"
    fi
    if [[ ! ${#password} -gt 7 ]]; then
        echo "Password must be at least 8 characters long"
    fi
    if [[ ! "$password" =~ [a-z] ]]; then
        echo "Password must contain at least one lowercase letter"
    fi
    if [[ ! "$password" =~ [A-Z] ]]; then
        echo "Password must contain at least one uppercase letter"
    fi
    if [[ ! "$password" =~ [0-9] ]]; then
        echo "Password must contain at least one number"
    fi
    if [[ "$password" == "chicken" ]]; then
        echo "Password must not contain the word 'chicken'"
    fi
    if [[ "$password" == "ernie" ]]; then
        echo "Password must not contain the word 'ernie'"
    fi

     read password
done

echo "$(date +"%y/%m/%d %T") REGISTER: INFO User $username registered successfully" >> ./users/log.txt
echo $username $password >> ./users/users.txt
