#!/bin/bash

sort -t ',' -k 1n '2023 QS World University Rankings.csv' | awk -F ',' '/JP/{print$1,$2}' | head -5
printf "\n" 
awk -F ',' '/JP/{print$9,$2}' '2023 QS World University Rankings.csv' | sort -t ',' -g | head -5
printf "\n"
sort -t ',' -k 20n '2023 QS World University Rankings.csv' | awk -F ',' '/JP/{print$20,$2}' | head -10
printf "\n"
grep -i 'keren' '2023 QS World University Rankings.csv' | awk -F ',' '{print$2}'
printf "\n"
